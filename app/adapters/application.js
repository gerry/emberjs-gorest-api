import RESTAdapter from '@ember-data/adapter/rest'
import { computed } from '@ember/object';
import ENV from 'emberjs-gorest-api/config/environment';

export default class ApplicationAdapter extends RESTAdapter {
  host = 'https://gorest.co.in';
  namespace = 'public-api';

  @computed
  get headers(){
    return {
      'Authorization': `Bearer ${ENV.GO_REST_API_TOKEN}`
    };
  }
}
