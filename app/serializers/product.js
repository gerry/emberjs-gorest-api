import ApplicationSerializer from './application';

export default class ProductSerializer extends ApplicationSerializer {
  modelNameFromPayloadKey(payloadKey) {
    if (payloadKey === 'data') {
      return super.modelNameFromPayloadKey(payloadKey.replace('data', 'product'));
    } else {
     return super.modelNameFromPayloadKey(payloadKey);
    }
  }
}
