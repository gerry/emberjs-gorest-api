import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default class ProductsRoute extends Route {
  @service store;
  queryParams = {
    page: {
      refreshModel: true
    }
  }

  model(params){
    return this.store.query('product', params);
  }
}
