import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';

export default class FormsPostEditComponent extends Component {
  @service router;

  @action
  cancel(){
    this.router.transitionTo('posts');
  }

  @action
  save(){
    let post = this.args.post;
    if(post.hasDirtyAttributes){
      post.save().then(result => {
        this.router.transitionTo('posts');
      });
    }
  }
}
