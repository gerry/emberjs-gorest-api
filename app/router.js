import EmberRouter from '@ember/routing/router';
import config from 'emberjs-gorest-api/config/environment';

export default class Router extends EmberRouter {
  location = config.locationType;
  rootURL = config.rootURL;
}

Router.map(function() {
  this.route('users', function(){
    this.route('edit', { path:'/:user_id' });
    this.route('new');
  });
  this.route('posts', function(){
    this.route('edit', { path: '/:post_id' });
  });
  this.route('products', function(){
    this.route('edit', { path: '/:product_id' });
  });
  this.route('comments', function(){
    this.route('edit', { path: '/:comment_id'})
  });
});
