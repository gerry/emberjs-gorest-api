import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

export default class FormsUserEditComponent extends Component {
  @service router;

  @action
  cancel(){
    this.router.transitionTo('users');
  }

  @action
  save(){
    let user = this.args.user;
    if(user.hasDirtyAttributes){
      user.save().then(result => {
        this.router.transitionTo('users');
      })
    }
  }
}
