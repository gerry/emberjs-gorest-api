import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default class UsersRoute extends Route {
  @service store;
  queryParams = {
    page: {
      refreshModel: true
    }
  }
  async model(params){
    return this.store.query('user', params);
  }
}
