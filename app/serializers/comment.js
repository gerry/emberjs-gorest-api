import ApplicationSerializer from './application';

export default class CommentSerializer extends ApplicationSerializer {
  modelNameFromPayloadKey(payloadKey) {
    if(payloadKey === 'data') {
      return super.modelNameFromPayloadKey(payloadKey.replace('data', 'comment'));
    }
    else {
      return super.modelNameFromPayloadKey(payloadKey);
    }
  }
}
