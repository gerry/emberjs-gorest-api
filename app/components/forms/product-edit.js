import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';

export default class FormsProductEditComponent extends Component {
  @service router;

  @action
  cancel(){
    this.router.transitionTo('products');
  }

  @action
  save(){
    let product = this.args.product;
    if(product.hasDirtyAttributes){
      product.save().then(result => {
        this.router.transitionTo('products');
      });
    }
  }
}
