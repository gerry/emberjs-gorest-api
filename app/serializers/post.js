import ApplicationSerializer from './application';

export default class PostSerializer extends ApplicationSerializer {
  modelNameFromPayloadKey(payloadKey) {
    if (payloadKey === 'data') {
      return super.modelNameFromPayloadKey(payloadKey.replace('data', 'post'));
    } else {
     return super.modelNameFromPayloadKey(payloadKey);
    }
  }
}
