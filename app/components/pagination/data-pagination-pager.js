import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';

export default class PaginationDataPaginationPagerComponent extends Component {
  @service router;

  @action
  pageChanged(current, previous){
    console.log(`current: ${current} previous: ${previous}`);
    this.router.transitionTo({
      queryParams: { page: current }
    });
  }
}
