import Model, { attr } from '@ember-data/model';

export default class ProductModel extends Model {
  @attr name;
  @attr description;
  @attr image;
  @attr price;
  @attr discount_amount;
  @attr status;
}
