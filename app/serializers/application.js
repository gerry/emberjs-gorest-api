import RESTSerializer from '@ember-data/serializer/rest';
import { merge } from '@ember/polyfills';

export default class ApplicationSerializer extends RESTSerializer {
  extractMeta(store, typeClass, payload){
    if (payload && payload.meta && payload.meta.hasOwnProperty('pagination')){
      let meta = payload.meta.pagination;
      delete payload.meta.pagination;
      return meta;
    }
  }

  serializeIntoHash(data, type, record, options){
    merge(data, this.serialize(record, options));
  }
}
