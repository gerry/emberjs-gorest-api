import Model, { attr } from '@ember-data/model';

export default class CommentModel extends Model {
  @attr post_id;
  @attr name;
  @attr email;
  @attr body;
}
