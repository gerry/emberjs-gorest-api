import Application from 'emberjs-gorest-api/app';
import config from 'emberjs-gorest-api/config/environment';
import { setApplication } from '@ember/test-helpers';
import { start } from 'ember-qunit';

setApplication(Application.create(config.APP));

start();
