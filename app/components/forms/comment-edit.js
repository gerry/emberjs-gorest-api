import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';

export default class FormsCommentEditComponent extends Component {
  @service router;

  @action
  cancel(){
    this.router.transitionTo('comments');
  }

  @action
  save(){
    let comment = this.args.comment;
    if(comment.hasDirtyAttributes){
      comment.save().then(result => {
        this.router.transitionTo('comments');
      })
    }
  }
}
