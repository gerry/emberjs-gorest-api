import ApplicationSerializer from './application';

export default class UserSerializer extends ApplicationSerializer {
  modelNameFromPayloadKey(payloadKey) {
    if (payloadKey === 'data') {
      return super.modelNameFromPayloadKey(payloadKey.replace('data', 'user'));
    } else {
     return super.modelNameFromPayloadKey(payloadKey);
    }
  }
}
